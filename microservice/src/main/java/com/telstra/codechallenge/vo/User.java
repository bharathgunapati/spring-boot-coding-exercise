package com.telstra.codechallenge.vo;

import lombok.Data;

import java.util.List;

@Data
public class User {

    private long total_count;
    private boolean incomplete_results;
    private List<Item> items;

}

package com.telstra.codechallenge.clients;

import com.telstra.codechallenge.config.AppConfig;
import com.telstra.codechallenge.vo.Item;
import com.telstra.codechallenge.vo.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Component
public class RestClient {

    private RestTemplate restTemplate;
    private AppConfig appConfig;

    public RestClient(RestTemplate restTemplate, AppConfig appConfig) {
        this.restTemplate = restTemplate;
        this.appConfig = appConfig;
    }

    public List<Item> getOldestUsersWithFollowers(Integer usersCount, Integer followersCount, Integer pageCount) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.parseMediaType("application/vnd.github.v3+json")));
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
        ResponseEntity<User> usersEntity = restTemplate.exchange(constructURI(usersCount, followersCount, pageCount),
                HttpMethod.GET, requestEntity, User.class);
        return usersEntity.getBody().getItems();
    }

    private String constructURI(Integer usersCount, Integer followersCount, Integer pageCount) {
        StringBuilder uriBuilder = new StringBuilder();
        uriBuilder.append(appConfig.getUserSearchUrl());
        uriBuilder.append("?");
        uriBuilder.append("q=followers:" + followersCount);
        uriBuilder.append("&per_page=" + usersCount);
        uriBuilder.append("&page=" + pageCount);
        uriBuilder.append("&sort=joined&order=asc");
        return uriBuilder.toString();
    }

}

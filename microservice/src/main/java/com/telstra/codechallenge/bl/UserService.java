package com.telstra.codechallenge.bl;

import com.telstra.codechallenge.api.response.ItemResponse;

import java.util.List;

public interface UserService {

    List<ItemResponse> getOldestUsersWithFollowers(Integer usersCount, Integer followersCount);

}

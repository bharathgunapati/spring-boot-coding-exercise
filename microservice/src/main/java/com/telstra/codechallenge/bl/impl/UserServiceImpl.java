package com.telstra.codechallenge.bl.impl;

import com.telstra.codechallenge.api.response.ItemResponse;
import com.telstra.codechallenge.bl.UserService;
import com.telstra.codechallenge.clients.RestClient;
import com.telstra.codechallenge.vo.Item;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    public static final int MAX_PAGE_SIZE = 100;
    private RestClient restClient;

    @Autowired
    public UserServiceImpl(RestClient restClient) {
        this.restClient = restClient;
    }


    @Override
    public List<ItemResponse> getOldestUsersWithFollowers(Integer usersCount, Integer followersCount) {
        List<ItemResponse> itemResponses = new ArrayList<>(usersCount);
        int pageCount = calculatePageCount(usersCount);
        if (pageCount > 1) {
            //since github search api is giving max of 100 results, need to loop and find the 100 results at once.
            for (int i = 1; i <= pageCount; i++) {
                itemResponses.addAll(getItems(usersCount, followersCount, i));
                usersCount = usersCount - MAX_PAGE_SIZE;
            }
        } else {
            itemResponses.addAll(getItems(usersCount, followersCount, pageCount));
        }
        return itemResponses;
    }

    private int calculatePageCount(Integer usersCount) {
        int pageCount;
        int pageSize = MAX_PAGE_SIZE;
        if (pageSize < MAX_PAGE_SIZE) {
            pageCount = 1;
        } else {
            if (usersCount % pageSize == 0) {
                pageCount = usersCount / pageSize;
            } else {
                pageCount = usersCount / pageSize + 1;
            }
        }
        return pageCount;
    }

    private List<ItemResponse> getItems(Integer usersCount, Integer followersCount, Integer pageCount) {
        List<ItemResponse> itemResponses = new ArrayList<>(usersCount);
        List<Item> items = restClient.getOldestUsersWithFollowers(usersCount, followersCount, pageCount);
        if (CollectionUtils.isNotEmpty(items)) {
            itemResponses = items.stream().map(item -> {
                ItemResponse itemResponse = new ItemResponse();
                BeanUtils.copyProperties(item, itemResponse);
                return itemResponse;
            }).collect(Collectors.toList());
        }
        return itemResponses;
    }
}

package com.telstra.codechallenge.controller;

import com.telstra.codechallenge.api.response.ItemResponse;
import com.telstra.codechallenge.bl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/old/no-followers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getOldestUsers(@RequestParam(name = "number") Integer number) {
        if (number < 0) {
            return ResponseEntity.badRequest().body("Number should be a positive integer");
        }
        if(number == 0) {
            return ResponseEntity.ok().body(new ArrayList<>());
        }
        List<ItemResponse> items = userService.getOldestUsersWithFollowers(number, 0);
        return ResponseEntity.ok().body(items);
    }
}

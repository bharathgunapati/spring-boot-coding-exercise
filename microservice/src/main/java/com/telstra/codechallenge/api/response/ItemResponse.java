package com.telstra.codechallenge.api.response;


import lombok.Data;

@Data
public class ItemResponse {

    private String login;
    private long id;
    private String html_url;
}

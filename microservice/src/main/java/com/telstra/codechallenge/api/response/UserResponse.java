package com.telstra.codechallenge.api.response;

import com.telstra.codechallenge.vo.Item;
import lombok.Data;

import java.util.List;

@Data
public class UserResponse {
    private long total_count;
    private boolean incomplete_results;
    private List<Item> items;
}

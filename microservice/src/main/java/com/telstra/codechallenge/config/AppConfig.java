package com.telstra.codechallenge.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class AppConfig {

    @Value("${users.search.url}")
    private String userSearchUrl;


}
